from django.contrib import admin

from json_fields.forms import JSONModelAdminForm
from product_catalog.models.product import Product


@admin.register(Product)
class JSONModelAdmin(admin.ModelAdmin):
    form = JSONModelAdminForm
