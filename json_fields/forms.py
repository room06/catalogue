from django import forms
from django_admin_json_editor import JSONEditorWidget

from product_catalog.models.product import Product

DATA_SCHEMA = {
    "title": "Data",
    "type": "object",
    "properties": {
        "fields": {
            "type": "array",
            "title": "Fields",
            "format": "tabs",
            "items": {
                "anyOf": [{
                    "title": "String",
                    "type": "string",
                }, {
                    "title": "Rich text field",
                    "type": "string",
                    "format": "html",
                    "options": {
                        "wysiwyg": "true"
                    },

                },
                    {
                        "title": "Number",
                        "type": "number",
                    },
                    {
                        "title": "Integer",
                        "type": "integer",
                    },
                    {
                        "type": "string",
                        "title": "Enum",
                        "enum": [
                            "value_1",
                            "value_2"
                        ]
                    },
                    {
                        "title": "select",
                        "type": "array",
                        "format": "select",
                        "uniqueItems": "true",
                        "items": {
                            "type": "string",
                            "enum": ["value1", "value2"]
                        }
                    },
                    {
                        "title": "checkboxes",
                        "type": "array",
                        "uniqueItems": "true",
                        "items": {
                            "type": "string",
                            "enum": ["value1", "value2"]
                        }
                    }
                ]
            }
        }
    }
}


class JSONModelAdminForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'dynamic_field': JSONEditorWidget(DATA_SCHEMA, collapsed=False, sceditor=True),
        }
