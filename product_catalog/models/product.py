# -*- coding: utf-8 -*-
""" Product Catalog: Product model """

from product_catalog.models.product_abstract import AbstractProduct


class Product(AbstractProduct):
    pass
