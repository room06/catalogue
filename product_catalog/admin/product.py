# # -*- coding: utf-8 -*-
""" ProductAdmin for Product Catalog """

from django.contrib import admin
from django.core.urlresolvers import NoReverseMatch
from django.utils.html import conditional_escape
from django.utils.html import format_html_join, format_html
from django.utils.translation import ugettext_lazy as _

from product_catalog.admin.forms import CategoryAdminForm


class ProductAdmin(admin.ModelAdmin):
    form = CategoryAdminForm

    def __init__(self, model, admin_site):
        self.form.admin_site = admin_site
        super(ProductAdmin, self).__init__(model, admin_site)
